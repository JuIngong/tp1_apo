package controlleur;

import java.io.IOException;
import java.sql.Date;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import test.User;
import test.UserDAO;

/**
 * Servlet implementation class Registration
 */
@WebServlet("/Registration")
public class Registration extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Registration() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String prenom = request.getParameter("name");
		String nom = request.getParameter("familyname");
		String date = request.getParameter("birthday");
		String genre = request.getParameter("gender");
		String tel = request.getParameter("tel");
		String email = request.getParameter("email");
		String id = request.getParameter("login");
		String mdp = request.getParameter("password");

		User test = new User(nom, prenom, email, id, mdp, tel, genre, date);

		UserDAO.requete(test);
		getServletContext().getRequestDispatcher("/index.jsp").forward(request,
				response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String function = request.getParameter("function");

		if (function.equals("getRegister")) {
			String prenom = request.getParameter("name");
			String nom = request.getParameter("familyname");

			int day = Integer.parseInt(request.getParameter("date_day"));
			int month = Integer.parseInt(request.getParameter("date_month"));
			int year = Integer.parseInt(request.getParameter("birthday_year"));
			Calendar c = Calendar.getInstance();
			c.set(c.YEAR, year);
			c.set(c.MONTH, month);
			c.set(c.DATE, day);
			String dateN = Date.valueOf(
					c.get(c.YEAR) + "-" + c.get(c.MONTH) + "-" + c.get(c.DATE))
					.toString();

			String genre = request.getParameter("Gender");
			String tel = request.getParameter("tel");
			String email = request.getParameter("email");
			String id = request.getParameter("login");
			String mdp = request.getParameter("password");
			String cfMdp = request.getParameter("cfpassword");
			//String passwd = UserDAO.getHash(request.getParameter("password"));
			//System.out.println(passwd);
			//String verifPasswd = UserDAO.getHash(request.getParameter("cfpassword"));
			//System.out.println(verifPasswd);
			// String kMdp = UserDAO.getHash(mdp);

			if (mdp.equals(cfMdp)) {
				User test = new User(nom, prenom, email, id, mdp, tel, genre,
						dateN);
				UserDAO.requete(test);
				getServletContext().getRequestDispatcher("/index.jsp").forward(
						request, response);
			} else {
				getServletContext().getRequestDispatcher("/uError.html")
						.forward(request, response);

			}

		}

	}

}
