package controlleur;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import test.UserDAO;
import test.User;

/**
 * Servlet implementation class Registration
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		User usr;

		String login = request.getParameter("username");
		String password = request.getParameter("password");

		try {
			usr = UserDAO.Verif(login, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			usr = null;
		}
		HttpSession session = request.getSession(true);
		if (usr == null) {
			request.setAttribute("login", login);
			getServletContext().getRequestDispatcher("/iError.jsp")
					.forward(request, response);
		} else {
			// Redirige vers la page de profil
			session.setAttribute("user", usr);
			getServletContext().getRequestDispatcher("/profil.jsp").forward(
					request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		User usr;
		String function = request.getParameter("function");
		if (function.equals("getLogin")) {

			String login = request.getParameter("username");
			String password = request.getParameter("password");

			try {
				usr = UserDAO.Verif(login, password);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				usr = null;
			}
			HttpSession session = request.getSession(true);
			if (usr == null) {
				request.setAttribute("login", login);
				getServletContext().getRequestDispatcher("/iError.jsp")
						.forward(request, response);
			} else {
				// Redirige vers la page de profil
				session.setAttribute("user", usr);
				getServletContext().getRequestDispatcher("/profil.jsp")
						.forward(request, response);
			}
		}
	}
}
