package test;


public class User {

	private String nom;
	private String prenom;
	private String email;
	private String id;
	private String mdp;
	private String tel;
	private String genre;
	private String dateN;



	public User(String nom, String prenom, String email, String id, String mdp,
			String tel, String genre, String dateN) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.id = id;
		this.mdp = mdp;
		this.tel = tel;
		this.genre = genre;
		this.dateN = dateN;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getDateN() {
		return dateN;
	}

	public void setDateN(String dateN) {
		this.dateN = dateN;
	}



}
