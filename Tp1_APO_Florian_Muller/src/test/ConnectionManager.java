package test;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import test.User;

/**
 *
 * @author Bouzidi
 */
public class ConnectionManager {

    public Connection conn;
    private Statement statement;
    public static ConnectionManager db;
    private ResultSet res;
    String url = "jdbc:mysql://localhost:3306/";
    String dbName = "apo_project";
    String driver = "com.mysql.jdbc.Driver";
    String userName = "root";
    String password = "";

    private ConnectionManager() {
        try {
            Class.forName(driver).newInstance();
            this.conn = (Connection) DriverManager.getConnection(url + dbName, userName, password);
        } catch (Exception sqle) {
            sqle.printStackTrace();
        }
    }

    /**
     *
     * @return MysqlConnect Database connection object
     */
    public static synchronized ConnectionManager getDbCon() {
        if (db == null) {
            db = new ConnectionManager();
        }
        return db;

    }

    /**
     *
     * @param query String The query to be executed
     * @return a ResultSet object containing the results or null if not
     * available
     * @throws SQLException
     */
    public ResultSet select(String query) throws SQLException {
        statement = db.conn.createStatement();
        ResultSet res = statement.executeQuery(query);
        db.conn.close();
        return res;
    }

    /**
     * @desc Method to insert data to a table
     * @param insertQuery String The Insert query
     * @return boolean
     * @throws SQLException
     */
    private void close() throws SQLException {
        db.conn.close();
    }

    public int insert(String insertQuery) throws SQLException {
        statement = db.conn.createStatement();
        int result = statement.executeUpdate(insertQuery);
        close();
        return result;

    }
    public User log(String insertQuery) throws SQLException {
    	conn = (Connection) DriverManager.getConnection(url + dbName, userName, password);
        statement = db.conn.createStatement();
        res = statement.executeQuery(insertQuery);

		res.first();
		String id = res.getString("Login");
		String nom = res.getString("Nom");
		String prenom = res.getString("Prenom");
		String date = res.getString("DateNaissance");
		String genre = res.getString("Genre");
		String tel = res.getString("Tel");
		String email = res.getString("email");
		String mdp = res.getString("mdp");
		
		close();
		
		User usr = new User(nom, prenom, email, id, mdp, tel, genre,
				date);
        
        return usr;

    }
}