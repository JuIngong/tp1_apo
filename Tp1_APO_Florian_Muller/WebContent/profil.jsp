<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Profil</title>
<link rel="stylesheet" type="text/css" href="all.css" />
</head>
<body>
	<script>
 window.fbAsyncInit = function() {
	  FB.init({
	    appId      : '270429599770205', // App ID
	    channelUrl : 'http://localhost:8080/Tp1_APO/Channel.html', // Channel File
	    status     : true, // check login status
	    cookie     : true, // enable cookies to allow the server to access the session
	    xfbml      : true  // parse XFBML
		  });
		
		  FB.Event.subscribe('auth.authResponseChange', function(response) {
		    if (response.status === 'connected') {
		    	 FB.Event.subscribe("auth.logout", function() {window.location = '/Tp1_APO/index.jsp'});
	    } else if (response.status === 'not_authorized') {
	      FB.login();
	    } else {
	      FB.login();
	    }
		  });
		  };
	
	  // Load the SDK asynchronously
	  (function(d){
	   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	   if (d.getElementById(id)) {return;}
	   js = d.createElement('script'); js.id = id; js.async = true;
	   js.src = "//connect.facebook.net/en_US/all.js";
	   ref.parentNode.insertBefore(js, ref);
	  }(document));
	  </script>
	<%@page import="test.*"%>
	<% User usr = (User)session.getAttribute("user");%>
	<h1>
		Profil de
		<%= usr.getId()%></h1>
	<br /> Nom :
	<%= usr.getNom() %><br>
	<br /> Prenom :
	<%= usr.getPrenom()%><br>
	<br /> Date de naissance :
	<%= usr.getDateN() %><br>
	<br /> Genre :
	<%= usr.getGenre() %><br>
	<br /> Email :
	<%= usr.getEmail() %><br>
	<br /> Tel :
	<%= usr.getTel() %><br>
	<br />
	<input type="button" value="Sign out"
		onClick="window.location.href='index.jsp'">
	<fb:login-button autologoutlink="true"></fb:login-button>
</body>
</html>