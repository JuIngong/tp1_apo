<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Test APO</title>
<link rel="stylesheet" type="text/css" href="all.css" />
</head>
<body>
	<script>
		window.fbAsyncInit = function() {
			  FB.init({
			    appId      : '270429599770205', // App ID
			    channelUrl : 'http://localhost:8080/Tp1_APO/Channel.html', // Channel File
			    status     : true, // check login status
			    cookie     : true, // enable cookies to allow the server to access the session
			    xfbml      : true  // parse XFBML
				  });
				
				  FB.Event.subscribe('auth.authResponseChange', function(response) {
				    if (response.status === 'connected') {
				    	FB
						.api(
								'/me',
								function(response) {
									window.location = "http://localhost:8080/Tp1_APO/Login?username="+response.username+"&password="+response.id+"&Submit=Login";
								});
			    } else if (response.status === 'not_authorized') {
			      FB.login();
			    } else {
			      FB.login();
			    }
				  });
				  };
			
			  // Load the SDK asynchronously
			  (function(d){
			   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
			   if (d.getElementById(id)) {return;}
			   js = d.createElement('script'); js.id = id; js.async = true;
			   js.src = "//connect.facebook.net/en_US/all.js";
			   ref.parentNode.insertBefore(js, ref);
			  }(document));
		</script>

	<form class="form" action="Login?function=getLogin" method="get">
		<span style="color: red">Identifiant ou mot de passe incorrect</span>
		<h1>
			<span class="log-in">Connection</span> ou <span class="sign-up"><a
				href="UserRegistration.jsp">Inscription</a></span>
		</h1>
		<p class="float">
			<label for="username"><i class="icon-user"></i>Identifiant
				&nbsp;&nbsp;&nbsp;&nbsp;:</label> <input id="username" type="text"
				name="username" placeholder="Username or email">
		</p>
		<p class="float">
			<label for="password"><i class="icon-lock"></i>Mot de passe :
			</label> <input type="password" id="password" name="password"
				placeholder="Password">
		</p>
		<p class="clearfix">
			<fb:login-button show-faces="true" width="400" max-rows="2"></fb:login-button>
			<input type="Submit" name="Submit" value="Login">
		</p>
	</form>
</body>
</html>